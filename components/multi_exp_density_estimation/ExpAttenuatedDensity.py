# (C) 2021 thpit. https://codeberg.org/thpit/IPiEM

import numpy as np

# Changelog
#
# 2021-08-31c:
# - corrected bug with regard to max i value:
#   "k*(L-1) + 1" --> "k*L"
#
# 2021-08-31b:
# - print() -> print_summary()
#
# 2021-08-30a:
# - gradient of obj function, and gradient descend update and
#   optim iteration implemented
#   ... towards testing
#
# 2021-08-28:
# - start
# - missing: 
#   gradient of obj function,
#   constraint related ortho space
#   projection

class ProblemSpec:
    # Actually intended: "ParameterSpec", but since dtilde_p_k_tgt
    # is included, use a wider name.
    def __init__(self, L, a_0, b, dtilde_p_k_tgt, vec_w):
        self.L = L
        self.a_0 = a_0
        self.b = b
        self.dtilde_p_k_tgt = dtilde_p_k_tgt    # expects this as vector
        self.vec_w = vec_w                      # expects this as weight vector for the error func
        self.K = dtilde_p_k_tgt.shape[0]
        self.epsilon = 0.000001

class ExpAttenuatedDensity:
    def __init__(self, L, a):
        self.L = L
        self.a = a
        self.gamma = np.zeros((L,)) # length L,
                                    # thus denoting coeffs for monomials 0 to L-1    

    def init_density_deprecated(self, a, do_normalize = False):
        """
        A trivial non-zero initialization.
        """
        self.a = a
        self.gamma = self.L - np.arange(self.L)
        self.gamma[0] = 1.0
        if do_normalize:
            self.normalize_gamma()
        
    def print_summary(self):
        print("gamma:")
        print(self.gamma)
        
    def get_eq_constraint_gradient(self):
        """
        Returns the gradient for 0 = -1 + \sum_{i=0}^{L-1} i!/(a^{i+1}) \gamma_i.
        """

        res = np.zeros((self.L,))        
        fac = 1.0
        for i in range(self.L):
            fac *= 1.0/self.a
            if i > 1:
                fac *= i
            res[i] = fac
        return res

    def normalize_gamma(self, do_debug=False):
        """
        It must be 1 = \sum_i=0^{L-1} \gamma_i i!/a^{i+1}
        """
        
        sum1 = np.dot(self.get_eq_constraint_gradient(), self.gamma)
        if do_debug: print("normalize_gamma(): Original integral value: ", sum1)
        self.gamma = self.gamma / sum1
        
    @staticmethod
    def evaluate_with_eta_and_a(x, eta, a):
        """
        Evaluates \sum_i=0 eta_i x^i * \exp(-a x) for
        whatever length eta has.
        """
        if x < 0.0: return 0.0
        if x <= 0.0: return eta[0]
        sum1 = 0.0
        fac = 1.0
        for i in range(eta.shape[0]):
            sum1 += eta[i] * fac
            fac *= x
        return sum1 * np.exp(- a * x)
        
    def evaluate(self, x):
        return self.evaluate_with_eta_and_a(x, self.gamma, self.a)        
    
        # old:        
        if x < 0.0: return 0.0
        if x <= 0.0: return self.gamma[0]
        sum1 = 0.0
        fac = 1.0
        for i in range(self.L):
            sum1 += self.gamma[i] * fac
            fac *= x
        return sum1 * np.exp(- self.a * x)
        
    def sample(self, x_min, x_max, N):
        x_space = x_min + (x_max-x_min)*(np.arange(N)+0.0)/N
        ##print(x_space)
        ls = [self.evaluate(x) for x in x_space]
        return x_space, np.array(ls)
         
    def integrate_numerically(self, x_min, x_max, N):
        x_vals, y_vals = self.sample(x_min, x_max, N)
        return (0.5 * y_vals[0] + np.sum(y_vals[1:(N-1)]) + 0.5 * y_vals[N-1]) * (x_max-x_min)/N   

    #def integrate_autoconvs_numerically_deprec(self, x_min, x_max, N, autoconv_eta = None):
    #    K = autoconv_eta.shape[0]
    #    x_space = x_min + (x_max-x_min)*(np.arange(N)+0.0)/N
    #    res = np.zeros((K,))
    #    print("autoconv_eta:")
    #    print(autoconv_eta)
    #    for k in range(1, K):
    #        sum1 = np.sum([ExpAttenuatedDensity.evaluate_with_eta_and_a(x, autoconv_eta[k,:], self.a) for x in x_space])
    #        res[k] = sum1 * (x_max-x_min)/N
    #    return res
        
    def get_autoconvolutions(self, K):
        """
        Gets coeffs of auto-convolutions for k=1...K-1, available
        in res[k,:], k=1...K-1. I.e. res[1,0:L] = self.gamma.
        
        Returns of format K \times ((K-1)*L).
        """
        
        # \eta_i^{[k]} = (1/i) * \sum_{j=0}^{i-1} \eta_j^{[k-1]} \gamma_{i-1-j} * 1/\binom{i-1,j}
        
        # Note:   k     i_max                       range(...)
        #       ---------------------------------------------
        #         1     L-1
        #         2     L-1+L-1+1 = 2(L-1) + 1      2 * L
        #         3     3(L-1) + 2                  3 * L
        #         4     4(L-1) + 3                  4 * L  
        
        L = self.L
        res = np.zeros((K, (K-1)*L))
        res[1, 0:L] = self.gamma
        for k in range(2, K):
            fac_i = 1.0
            ##print("---- k=", k)
            for i in range(1, k*L):
                fac2 = 1.0          # init with (i-1)!  (fac2 realizes j! * (i-1-j)! )
                #fac_i *= i          # realizes i!
                sum2 = 0.0
                ##print("-- i=", i)
                for j in range(0, i): # j = 0...i-1
                    ##print("fac2: ", fac2, ('*' if j >= i-L else ' '), j)
                    #if j >= i-L:
                    #    sum2 += res[k-1, j] * self.gamma[i-1-j] * fac2       # "eta^[k-1]_{j} * gamma_{i-1-j} * j! * (i-1-j)!"
                    #        # It must be i-1-j <= L-1 in order to have a summand.
                    #        # For k=2, have also j <= L-1, thus i <= L-1+L-1 + 1 = k*(L-1) + k
                    if j < L:
                        sum2 += res[k-1, i-1-j] * self.gamma[j] * fac2       # "eta^[k-1]_{j} * gamma_{i-1-j} * j! * (i-1-j)!"
                    if (i > 1) and (j < i-1):
                        fac2 *= (j+1) / (i-1-j)
                res[k,i] = sum2 / i
        return res
        
    @staticmethod
    def sample_autoconvs(autoconv_eta, a, x_min, x_max, N):
        """
        Returns x_vals,y_vals
        where x_vals is N-dim vector, y_vals is K \times N.
        """
        x_space = x_min + (x_max-x_min)*(np.arange(N)+0.0)/N
        K = autoconv_eta.shape[0]  # nmb of convs
        y_vals = np.zeros((K, N))
        for k in range(1, K):
            ls = [ExpAttenuatedDensity.evaluate_with_eta_and_a(x, autoconv_eta[k,:], a) for x in x_space]
            y_vals[k,:] = np.array(ls)
        return x_space, y_vals
        
    def integrate_autoconvs_numerically(self, x_min, x_max, N, autoconv_eta = None, K = None):
        if autoconv_eta is None:
            autoconv_eta = self.get_autoconvolutions(K)
        x_space, y_vals = ExpAttenuatedDensity.sample_autoconvs(autoconv_eta, self.a, x_min, x_max, N)
        return np.sum(y_vals, axis=1) * (x_max-x_min)/N
        
    def get_autoconv_eta_gradients(self, autoconv_eta):
        """
        Gets the gradients of the coefficients of the auto-convs,
        evaluated at the current value of self.gamma.
        
        This is a third-order tensor, of format K \times ((K-1)*L) \times L.
        I.e. res[k,i,ll].
        
        Uses the autoconv_eta as precomputed values.
        """
        
        # \d \eta_i^{[k]} / \d \gamma_ll = \eta_{i-1-ll}^{[k-1]} * 1.0/\binom{i-1,ll}/i 
        #                                 + \sum_{j=0}^{i-1} \d \eta_{i-1-j}^{[k-1]} / \d \gamma_ll * \gamma_j 1.0/\binom{i-1,j}/i
        
        K = autoconv_eta.shape[0]
        L = self.L
        res = np.zeros((K, (K-1)*L, L))
        for ll in range(L):         # The gradient of \gamma wrt to \gamma is just the identity matrix.
            res[1, ll, ll] = 1     
        for k in range(2, K):
            for ll in range(L):
                ##print()
                for i in range(1, k*L):
                    sum2 = 0.0
                    fac2 = 1.0
                    ##print()
                    for j in range(0, i-1):
                        ##print("fac2: ", fac2)
                        if j < L:
                            sum2 += res[k-1, i-1-j, ll] * self.gamma[j] * fac2
                        if j==ll:
                            sum2 += autoconv_eta[k-1, i-1-ll] * fac2
                        if (i > 1) and (j < i-1):
                            fac2 *= (j+1) / (i-1-j)
                    res[k, i, ll] = sum2 / i
        return res
    
    @staticmethod    
    def get_E_i_ab(a, b, I):
        """
        Returns for all i=0...I-1
        
        Signifies the E_i(a,b) in  
        
            \int_b^\infty x^i * \exp(-a x) \d x = E_i(a,b) * \exp(-a b)
        """
        
        if b < 0.0: return 0.0
        
        res = np.zeros((I,))
        if b <= 0.0: 
            # Return for the case b==0:
            # For an i, the solution is i!/a^{i+1} 
            fac = 1.0
            for i in range(I):
                fac *= (1.0/a)
                if i > 1:
                    fac *= i
                res[i] = fac
            return res
            
        # Here b > 0:
        fac1 = 1.0          # represents b^{i+1}
        fac_i = 1.0
        fac_apowipo = 1.0   #  represents a^{i+1}
        ab_recip = 1.0/(a*b)
        for i in range(I):
            fac1 *= b       
            fac_apowipo *= a
            if i > 1: fac_i *= i
            sum2 = 0.0
            fac2 = ab_recip
            for h in range(0, i): 
                sum2 += fac2
                fac2 *= ab_recip * (i-h)
            res[i] = (sum2 * fac1 + fac_i/fac_apowipo)
        return res

    def get_residual_probabilities(self, b, autoconv_eta = None, K = None):
        """
        Returns the residu. probs for all autoconvs.
        
        Uses precomputed values in autoconv_eta if given, otherwise
        computes them. K must be non-None if autoconv_eta is None.
        
        """
        
        # ok. confirmed.
        
        if autoconv_eta is None:
            autoconv_eta = self.get_autoconvolutions(K)
        else:
            K = autoconv_eta.shape[0]
        
        # \int_b^\infty x^i \exp(- a x) \d x = b^{i+1} * \sum_{h=1}^{i+1} i!/(a b)^h (i-h)! * \exp(-a b) =: E_i(a, b) * \exp(-a b)
        # \int_b^\infty f^{*k}(x) \d x = \sum_{i=0}^\infty \eta_i^{[k]} * E_i(a,b) * \exp(- a b)
        
        L = self.L
        I = (K-1)*L
        E_i_ab = ExpAttenuatedDensity.get_E_i_ab(self.a, b, I)
        resid_probs = np.zeros((K,))
        for k in range(1, K):
            resid_probs[k] = np.dot(autoconv_eta[k,:], E_i_ab) * np.exp(- self.a * b)
        return resid_probs
        
    def get_resid_prob_grads(self, b, autoconv_eta = None, K = None):
        """
        Result is gradient at current values of self.gamma and self.a, 
        and is of format K \times L.
        
        autoconv_eta is used as precomputed if non-None, otherwise
        it is computed (using the specified K).
        
        In the result, res[0,:] is undefined.
        """
    
        if autoconv_eta is None:
            autoconv_eta = self.get_autoconvolutions(K)
        else:
            K = autoconv_eta.shape[0]
        L = self.L
        I = (K-1)*L
        
        E_i_ab = ExpAttenuatedDensity.get_E_i_ab(self.a, b, I)
        eta_grads = self.get_autoconv_eta_gradients(autoconv_eta)
        res = np.zeros((K,L))
        for k in range(1, K):
            for ll in range(L):
                res[k,ll] = np.dot(eta_grads[k,:,ll], E_i_ab) * np.exp(- self.a * b)
        return res
        
    def obj_function(self, prob_spec, tilde_p_k = None):
        """
        If the residual probabilities tilde_p_k are specified, uses these; otherwise
        computes these values.
        """
        
        K = prob_spec.K
        
        if tilde_p_k is None:
            tilde_p_k = self.get_residual_probabilities(prob_spec.b, None, K)
            
        buf = tilde_p_k - prob_spec.dtilde_p_k_tgt
        return np.sum(prob_spec.vec_w * buf * buf)
        
    @staticmethod
    def euclidean_length(v):
        return np.sqrt(np.sum(v*v))

    @staticmethod
    def eucl_normalize_vec(v):
        eucl_length = np.sqrt(np.sum(v*v))
        if eucl_length <= 0.0: return v
        return v / eucl_length
        
    @staticmethod
    def orthogonal_projection(v, U):
        """
        Expects in ortho_space (=:U) a matrix of format n x L, 
        where rows are the vectors spanning the space to which
        the result is to be orthogonal to. v is the vector to 
        be projected.
        
        Generally, if U_nrm denotes the matrix arising from
        U by normalizing all rows, then the projection operator
        is  P = I_{L,L} - U_nrm^T U_nrm, because for any u^T = row of U_nrm,
        say the j-th row, get 
            P u = u - U_nrm^T U_nrm u = u - U_nrm^T e_j = u - u = 0.
        """
        
        if len(U.shape) == 1:   # is vector
            U = ExpAttenuatedDensity.eucl_normalize_vec(U)
            return v - U * np.dot(U, v)
        else:                   # U is matrix
            for j in range(U.shape[0]):
                U[j,:] = ExpAttenuatedDensity.eucl_normalize_vec(U[j,:])
            return v - U.T @ (U @ v)
            
    def get_minimum_val(self, x_min, x_max, N):
        """
        Returns the minimum value the sampled density attains in [x_min,x_max],
        and the according gradient (ex \exp(-a x)) that can be used to
        alter gamma to change this part of the density.
        """
        x_vals, y_vals = self.sample(x_min, x_max, N)
        y_minval = np.min(y_vals)
        inds = np.where(y_vals <= y_minval)     # minimum places
        x_minval = x_min + (x_max-x_min) / N * inds[0]
        ##print("I: get_minimum_val(): res = (", x_minval, y_minval, ")")
        u_2 = np.ones((self.L,))
        for j in range(1,self.L):
            u_2[j] = u_2[j-1] * x_minval
        return y_minval, u_2
        
    def perform_update_step(self, prob_spec, curr_step_size, tilde_p_k = None, do_debug = False):
        # With 
        #   e(\vec\gamma,a) := \sum_{k=1}^{K-1} (\tilde p_k(\gamma, a) - \dtilde p_k)^2 * w_k,
        # compute the gradient of e and proceed along it,
        # using a step-size which is adaptively chosen 
        # to increase the descend.
        
        # The gradient is given by 
        #   \prtt{e}{\gamma_l} = 2 * \sum_{k=1}^{K-1} w_k * (\tilde p_k(\gamma, a) - \dtilde p_k) * \prtt{\tilde p_k}{\gamma_l}
        #
        # The approach with regards to step size is to compute 
        # the objective function along a few domain points
        # in direction of the negative gradient.
        
        K = prob_spec.K

        autoconv_eta = self.get_autoconvolutions(K)

        if tilde_p_k is None:
            tilde_p_k = self.get_residual_probabilities(prob_spec.b, autoconv_eta)
        
        objfval_curr = self.obj_function(prob_spec, tilde_p_k)
            
        resid_prob_grads = self.get_resid_prob_grads(prob_spec.b, autoconv_eta)     # format K \times L
        
        buf = tilde_p_k - prob_spec.dtilde_p_k_tgt
        if do_debug: print("per-k deviations: ", buf)
        grad_e = 2 * np.dot(prob_spec.vec_w * buf, resid_prob_grads)    # result is L-dim vector
        grad_e_length = ExpAttenuatedDensity.euclidean_length(grad_e) / np.sqrt(self.L)
        if do_debug: print("grad_e_length: ", grad_e_length)
        if grad_e_length < prob_spec.epsilon:
            return objfval_curr, objfval_curr, 0.0, 0.0 * self.gamma
        u = self.get_eq_constraint_gradient()
        grad_e_prj = ExpAttenuatedDensity.orthogonal_projection(grad_e, u)
        grad_e_prj_nrm = ExpAttenuatedDensity.eucl_normalize_vec(grad_e_prj)
        if do_debug: 
            print("grad_e_prj_nrm (1):")
            print(grad_e_prj_nrm)

        gamma_0 = 0.0 + self.gamma
        
        # Account for possible negativity of the new density function
        # by removing the crucial component from the gradient:
        if 1:
            # Use a candidate gamma in order to determine the neg val place:
            self.gamma = gamma_0 - 0.5 * curr_step_size * grad_e_prj_nrm
            x_max_ = prob_spec.b * 2.0
            x_space, y_vals = self.sample(0.0, x_max_, 200)
            y_minval = None
            x_minval = None
            for t in range(200):
                if (x_minval is None) or (y_vals[t] < y_minval):
                    y_minval = y_vals[t]
                    x_minval = x_max_ / 200 * t
            if y_minval < 0.0:
                if do_debug: print("I: Got a y_minval < 0")
                # Determine the constraint's gradient:
                # If f(x_minval) >= 0 is to be enforced, then apparently 
                # u_2 := (1, x_minval, x_minval^2, ...) is the gamma gradient of this.
                # Then ensure that any component v in grad_e with u_2^T v < 0
                # is "mostly" removed.
                u_2 = np.ones((self.L,))
                for j in range(1,self.L):
                    u_2[j] = u_2[j-1] * x_minval
                if do_debug: 
                    print("u_2:")
                    print(u_2)
                if np.dot(u_2, -grad_e_prj_nrm) < 0.0:   
                    # This should be always true, if the original density was non-neg everywhere
                    if do_debug: print("I: Original grad direction grad_e_prj_nrm has a component towards negative density")
                    u_2 = ExpAttenuatedDensity.eucl_normalize_vec(u_2)
                    grad_e_prj_nrm = 0.2 * grad_e_prj_nrm + 0.8 * ExpAttenuatedDensity.orthogonal_projection(grad_e, np.stack([u,u_2], axis=0))
                    grad_e_prj_nrm = ExpAttenuatedDensity.eucl_normalize_vec(grad_e_prj_nrm)
                    if do_debug: 
                        print("grad_e_prj_nrm (2):")
                        print(grad_e_prj_nrm)
                
        # Test various step sizes:
        nTests = 3
        gamma_cands = np.zeros((nTests, prob_spec.L))
        stsz = 0.5 * curr_step_size
        best_stsz = None
        best_t = None
        objfval_min = -1
        for t in range(nTests):
            gamma_cands[t,:] = gamma_0 - stsz * grad_e_prj_nrm
            self.gamma = gamma_cands[t,:]
            objfval = self.obj_function(prob_spec)
            if (objfval_min < 0.0) or (objfval < objfval_min):
                objfval_min = objfval
                best_stsz = stsz
                best_t = t
            stsz *= 2.0
        if do_debug: print("Best step size: ", best_stsz)
        self.gamma = gamma_cands[best_t,:]
        self.normalize_gamma(do_debug=do_debug)
        
        # Ensure all values are non-negative:
        if 0: # (A)
            val_min,_ = self.get_minimum_val(0.0, 10.0, 200)
            #print("val min: ", val_min)
            if val_min < 0.0:
                self.gamma[self.gamma < 0.0] *= 0.7     # decrease the magnitude of the negative coeffs
                self.normalize_gamma(do_debug=do_debug)
        if 0: # (B)
            val_min,_ = self.get_minimum_val(0.0, 10.0, 200)
            #print("val min: ", val_min)
            if val_min < 0.0:
                if do_debug: print("I: Reshifting gamma coeffs in order to ensure non-negativity...")
                self.gamma[0] -= val_min                # makes gamma[0] == 0.0
                self.normalize_gamma()
        if 0: # (C)
            val_min, u_2 = self.get_minimum_val(0.0, 10.0, 200)
            #print("val min: ", val_min)
            if val_min < 0.0:
                if do_debug: print("I: Reshifting gamma coeffs in order to ensure non-negativity...")
                self.gamma += u_2 / np.dot(u_2, u_2) * (-val_min)
                self.normalize_gamma()
        
        return objfval_curr, objfval_min, best_stsz, self.gamma - gamma_0
            
    def optimize_by_gradient_descend(self, prob_spec, step_size_0 = 0.1, gamma_0 = None, do_debug = False):
        nCycles = 100
        if not (gamma_0 is None):
            self.gamma = 0.0 + gamma_0
            
        curr_step_size = step_size_0
        for cycle in range(nCycles):
            if do_debug: print("============================================== ", cycle)
            objfval_prev, objfval_min, best_stsz, delta_gamma = self.perform_update_step(prob_spec, curr_step_size, None, do_debug)
            if do_debug: 
                print("objfval_prev, objfval_min, best_stsz: ", objfval_prev, objfval_min, best_stsz)
                print("delta_gamma: ", delta_gamma)
                print("gamma: ", self.gamma)
                if cycle % 10 == 0:
                    self.plot_density(0.0, self.a*4.0, "Density at cycle {}".format(cycle))
            curr_step_size = best_stsz
        return self.gamma
        
    def plot_density(self, x_min, x_max, caption, N = 1000):
        if x_min is None:
            x_min = 0.0
            x_max = self.a * 4.0
            caption = ""
        x_vals, y_vals = self.sample(x_min, x_max, N)
        
        import matplotlib.pyplot as plt
        plt.plot(x_vals, y_vals)
        plt.title(caption)
        plt.show()
        
