import numpy as np

from ExpAttenuatedDensity import ExpAttenuatedDensity
from ExpAttenuatedDensity import ProblemSpec


if __name__ == "__main__":

    if 1:
        L = 8
        a = 5.0
        dens1 = ExpAttenuatedDensity(L, a)
        dens1.gamma[:] = 0.1
        dens1.gamma[L-1] = 4.0
        dens1.normalize_gamma()
        dens1.print_summary()

        b = 1.0
        K = 8
        autoconv_eta = dens1.get_autoconvolutions(K)
        tilde_p_k = dens1.get_residual_probabilities(b, autoconv_eta)
        print("resid_probs:")
        print(tilde_p_k)

        import matplotlib.pyplot as plt
        dens1.plot_density(0.0, 20.0, "Original density for target resid probs")

        
        if 0:        
            print("Numerical integral of f:             ", dens1.integrate_numerically(0.0, 30.0, 4000))
            print("Numerical integral of the autoconvs: ", dens1.integrate_autoconvs_numerically(0.0, 30.0, 4000, autoconv_eta))
        
        if 0:
            import matplotlib.pyplot as plt

            N = 200
            x_vals, y_vals = dens1.sample_autoconvs(autoconv_eta, dens1.a, 0.0, 20.0, N)
            for k in range(1, K):
                plt.plot(x_vals, y_vals[k,:], label='k={}'.format(k))
            plt.legend()
            plt.show()


        
        vec_w = 0.0 + K - np.arange(K)
        #vec_w = vec_w * vec_w
        print("vec_w:")
        print(vec_w)
        dtilde_p_k_tgt = 0.0 + tilde_p_k
            # We know the correct gamma value for this tgt residual prob vector -
            # the above gamma. Now use a different gamma as start val:

        # Set a new gamma:
        dens1.gamma[:] = L - np.arange(L)
        dens1.gamma[0] = 10.0
        dens1.normalize_gamma()
        print("gamma_0: (initialization of optimization)")
        print(dens1.gamma)

        import matplotlib.pyplot as plt

        dens1.plot_density(0.0, 20.0, "Density for the gamma initial value")

            
        if 0:
            import matplotlib.pyplot as plt

            dtilde_p_k_tgt = 0.0 + tilde_p_k
            dtilde_p_k_tgt[1] = 0.0
            dtilde_p_k_tgt[1] = 0.1
            dtilde_p_k_tgt[2] = 0.2
            dtilde_p_k_tgt[3] = 0.4
            dtilde_p_k_tgt[4] = 0.8
            pspec = ProblemSpec(L, a, b, dtilde_p_k_tgt, vec_w)
            dens1.optimize_by_gradient_descend(pspec, 0.1)
            
        if 1:
            pspec = ProblemSpec(L, a, b, dtilde_p_k_tgt, vec_w)
            gamma_est = dens1.optimize_by_gradient_descend(pspec, 0.1, None, do_debug=False)
            
            print()
            print("gamma_est:")
            print(gamma_est)
            
            tilde_p_k = dens1.get_residual_probabilities(b, None, K)
            print("Final resid_probs:")
            print(tilde_p_k)

            f_min,_ = dens1.get_minimum_val(0.0, 20.0, 400)
            print("Density min val: ", f_min)

