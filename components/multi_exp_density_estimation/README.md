# multi_exp_density_estimation

This project demonstrates (proof-of-concept stage) how
to obtain the probability density function given a finite sequence
of residual probabilities of its repeated convolution.

Given positive real values \tilde p_k, deemed to represent integrals 

    \int_b^\infty f^{*k}(y) \d y =: \tilde p_k,     k=0...K-1,

(Here f^{*k} is k-fold convolution, with f^{*1} = f.)
with b > 0 fixed, aim to determine f.


## (Experimentation) Usage

Adjust code in ExpAttenuatedDensity_run4.py, then 

    $ python3 ExpAttenuatedDensity_run4.py


## Issues

- algorithm in current form still prone to convergence to non-global local minima
- theoretical foundation missing


## License

There is currently no license file. Potential users shall make contact via e.g. 
codeberg "new issue".



